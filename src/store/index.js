import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    first_name: '',
    surname: '',
    job_title: '',
    email: '',
    phone: ''
  },
  mutations: {
    set_first_name (state, payload) {
      state.first_name = payload.first_name
    },
    set_surname (state, payload) {
      state.surname = payload.surname
    },
    set_job_title (state, payload) {
      state.job_title = payload.job_title
    },
    set_email (state, payload) {
      state.email = payload.email
    },
    set_phone (state, payload) {
      state.phone = payload.phone
    }
  },
  actions: {
  },
  modules: {
  }
})
